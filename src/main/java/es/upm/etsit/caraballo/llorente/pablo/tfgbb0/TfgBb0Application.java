package es.upm.etsit.caraballo.llorente.pablo.tfgbb0;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfgBb0Application {

	public static void main(String[] args) {
		SpringApplication.run(TfgBb0Application.class, args);
	}

}
