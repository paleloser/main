# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-06-07

### Added

#### TFGBBO

- [TFGBBO-1](https://palelo.atlassian.net/browse/TFGBBO-1) Integrar changelog-drafter en la gestión del CHANGELOG

#### TFGBBZ

- [TFGBBZ-2](https://palelo.atlassian.net/browse/TFGBBZ-2) Añadir filtro de autenticación
- [TFGBBZ-1](https://palelo.atlassian.net/browse/TFGBBZ-1) Definir controlador de HelloWorld

### Removed

#### TFGBBZ

- [TFGBBZ-3](https://palelo.atlassian.net/browse/TFGBBZ-3) Eliminar filtro de autenticación

[Unreleased]: https://bitbucket.org/projects/paleloser/repos/main/compare/commits?targetBranch=1.0.0&sourceBranch=refs/heads/develop
[1.0.0]: https://bitbucket.org/projects/paleloser/repos/main/commits?until=1.0.0
